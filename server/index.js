const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const app = express();
const port = 4000;

app.use(cors());

app.get("/", (req, res) => {
  const user = {
    name: "louis",
    age: 8,
  };
  res.status(200).send(user);
});

app.get("/date", (req, res) => res.status(200).send(new Date()));

app.listen(port, () => console.log("bien connecté sur le serveur"));
